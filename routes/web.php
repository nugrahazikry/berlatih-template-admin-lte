<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome3');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/register', 'Authcontroller@register');

Route::get('/welcome2', function () {
    return view('welcome2');
});

Route::get('/welcome2', 'Authcontroller@welcome2');
Route::post('/welcome2', 'Authcontroller@welcome2_post');


Route::get('/master', function() {
	return view('adminlte.master');
});

Route::get('/items', function(){
	return view('items.index');
});

Route::get('/items/create', function(){
	return view('items.create');
});

Route::get('/data-table', function() {
	return view('adminlte.data-table');
});